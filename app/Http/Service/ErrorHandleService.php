<?php

namespace App\Http\Service;

use App\Http\Controllers\Controller;

class ErrorHandleService extends Controller
{
    public function errorMessage($message, $status_code){
        return response()->json([
            'data' => [
                'message' => $message,
                'status_code' => $status_code
            ]
        ], $status_code);
    }
}