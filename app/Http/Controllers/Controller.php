<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function errorMessage($message, $status_code){
        return response()->json([
            'data' => [
                'message' => $message,
                'status_code' => $status_code
            ]
        ], $status_code);
    }

    public function successMessage($items=null, $message, $status_code){
        response()->json([
            'data' => [
                'items' => $items,
                'message' => $message,
                'status_code' => $status_code
            ]
        ], $status_code);
    }
}
