<?php

namespace App\Http\Controllers\Api\Auth;

use App\Model\UserModel;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     * @return User
     * @internal param array $data
     */
    public function register(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $pass = $request->pass;
        $user = new UserModel();
        $user->name = $name;
        $user->email = $email;
        $user->pass = password_hash($pass, PASSWORD_DEFAULT);
        if ($user->save()) {
            $this->successMessage([], "회원님 환영합니다.", Response::HTTP_CREATED);
        } else {
            $this->errorMessage("회원가입에 실패했습니다.", Response::HTTP_FAILED_DEPENDENCY);
        }
    }
}
