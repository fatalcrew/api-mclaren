<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Service\ErrorHandleService;
use App\Model\UserModel;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Firebase\JWT\JWT;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function getToken()
    {
        $key = file_get_contents("../config/jwt/private_rsa.pem");
        $nowTime = time();
        $token = [
            "email" => "witshm@gmail.com",
            "redirect_url" => "http://example.com",
            "iat" => $nowTime,
            "exp" => $nowTime+(60*50)
        ];
        $jwt = JWT::encode($token, $key);
        return response()->json([
            'data' => [
                'token' => $jwt,
                'message' => '성공',
                'status_code' => Response::HTTP_OK
            ]
        ], Response::HTTP_OK);
    }

    public function userCheck(Request $request)
    {
        $username = $request->username;
        $pass = $request->pass;
        $user = UserModel::where('email', $username)->first();
        if (!$username || !$pass) {
            return $this->errorMessage('아이디와 비밀번호는 필수값입니다.', Response::HTTP_UNAUTHORIZED);
        }
        if (!$user) {
            return $this->errorMessage("회원이 아닙니다.", Response::HTTP_UNAUTHORIZED);
        }
        if (password_verify($pass, $user->pass)) {
            return $this->errorMessage("비밀번호가 틀립니다.", Response::HTTP_UNAUTHORIZED);
        }

        return $this->getToken();
    }
}
